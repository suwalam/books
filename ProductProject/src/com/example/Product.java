package com.example;

import java.math.BigDecimal;

public abstract class Product implements PriceCalculable {
    protected String name;

    private BigDecimal basePrice;

    private int vatPercentage;

    private String category;

    private UnitOfMeasure unitOfMeasure;

    public Product(String name, BigDecimal basePrice, int vatPercentage, String category, UnitOfMeasure unitOfMeasure) {
        this(name, basePrice, vatPercentage);
        this.category = category;
        this.unitOfMeasure = unitOfMeasure;
    }

    public Product(String name, BigDecimal basePrice, int vatPercentage) {
        this.name = name;
        this.basePrice = basePrice;
        this.vatPercentage = vatPercentage;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public BigDecimal getBasePrice() {
        return basePrice;
    }
    public void setBasePrice(BigDecimal basePrice) {
        this.basePrice = basePrice;
    }
    public int getVatPercentage() {
        return vatPercentage;
    }
    public void setVatPercentage(int vatPercentage) {
        this.vatPercentage = vatPercentage;
    }
    public String getCategory() {
        return category;
    }
    public void setCategory(String category) {
        this.category = category;
    }
    public UnitOfMeasure getUnitOfMeasure() {
        return unitOfMeasure;
    }
    public void setUnitOfMeasure(UnitOfMeasure unitOfMeasure) {
        this.unitOfMeasure = unitOfMeasure;
    }
    public void print() {
        System.out.println("Product name: " + name + ", price: " + basePrice + ", vat: " + vatPercentage
                + ", category: " + category + ",unitOfMeasure: " + unitOfMeasure.getName());
    }

    protected BigDecimal calculateVat() {
        if (getVatPercentage() == 0) {
            return BigDecimal.ONE;
        }
        return BigDecimal.valueOf(1 + (getVatPercentage() / 100.0));
    }
}


