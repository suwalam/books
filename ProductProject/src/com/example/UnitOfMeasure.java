package com.example;

public enum UnitOfMeasure {

    HIS("tona", 1000), KG("kilogram", 1), PIECE("sztuka", 1),
    LITRE("litr", 1), ML("mililitr", 1000);

    private String name;

    private int multiplicand;

    UnitOfMeasure(String name, int multiplicand) {
        this.name = name;
        this.multiplicand = multiplicand;
    }

    public String getName() {
        return name;
    }

    public int getMultiplicand() {
        return multiplicand;
    }
}

