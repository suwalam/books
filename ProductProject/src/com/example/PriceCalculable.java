package com.example;

import java.math.BigDecimal;

public interface PriceCalculable {

    BigDecimal calculatePrice();
}

