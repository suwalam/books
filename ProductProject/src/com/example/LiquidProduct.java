package com.example;

import java.math.BigDecimal;

public class LiquidProduct extends Product {

    private double capacity;

    public LiquidProduct(String name, BigDecimal basePrice, int vatPercentage, String category,
                         UnitOfMeasure unitOfMeasure, double capacity) {
        super(name, basePrice, vatPercentage, category, unitOfMeasure);
        this.capacity = capacity;
    }

    public double getCapacity() {
        return capacity;
    }

    public void setCapacity(double capacity) {
        this.capacity = capacity;
    }

    @Override
    public BigDecimal calculatePrice() {
        return getBasePrice()
                .multiply(calculateMeasuredCapacity())
                .multiply(calculateVat())
                .setScale(2, BigDecimal.ROUND_HALF_UP);
    }

    @Override
    public void print() {
        System.out.println("Product name: " + getName() + ", price: " + getBasePrice() + ", vat: " + getVatPercentage()
                + ", category: " + getCategory() + ",unitOfMeasure: " + getUnitOfMeasure() + ", pojemność: " + capacity);
    }

    private BigDecimal calculateMeasuredCapacity() {
        return BigDecimal.valueOf(capacity / getUnitOfMeasure().getMultiplicand());
    }
}
