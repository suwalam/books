package com.example;

import java.math.BigDecimal;

public class LooseProduct extends Product {

    private double weight;

    public LooseProduct(String name, BigDecimal basePrice, int vatPercentage, String category,
                        UnitOfMeasure unitOfMeasure, double weight) {
        super(name, basePrice, vatPercentage, category, unitOfMeasure);
        this.weight = weight;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    @Override
    public BigDecimal calculatePrice() {
        return getBasePrice()
                .multiply(calculateMeasuredWeight())
                .multiply(calculateVat())
                .setScale(2, BigDecimal.ROUND_HALF_UP);
    }

    @Override
    public void print() {
        System.out.println("Product name: " + getName() + ", price: " + getBasePrice() + ", vat: " + getVatPercentage()
                + ", category: " + getCategory() + ",unitOfMeasure: " + getUnitOfMeasure() + ", waga: " + weight);
    }

    private BigDecimal calculateMeasuredWeight() {
        return BigDecimal.valueOf(weight / getUnitOfMeasure().getMultiplicand());
    }
}




