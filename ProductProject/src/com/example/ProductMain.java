package com.example;

import java.math.BigDecimal;

public class ProductMain {

    public static void main(String[] args) {

            Product looseProduct = new LooseProduct("cement", BigDecimal.valueOf(500), 23,
            "artykuł budowlany", UnitOfMeasure.HIS, 200);

        looseProduct.print();

        System.out.println(looseProduct.calculatePrice() + " zł");

        Product liquidProduct = new LiquidProduct("farba", BigDecimal.valueOf(24), 23,
                "artykuł budowlany", UnitOfMeasure.ML, 800);

        liquidProduct = looseProduct;

        liquidProduct.print();
        System.out.println(liquidProduct.calculatePrice() + " zł");
    }
}




